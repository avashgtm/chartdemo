<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Chart extends Model
{
	public function getDataAttribute($v){
		return unserialize($v);
	}
	public function setDataAttribute($v){
		$this->attributes['data'] = serialize($v);
	}
}