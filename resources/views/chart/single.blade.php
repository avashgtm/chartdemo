<!DOCTYPE html>
<html>
<head>
	<title>single chart</title>

</head>
<body>
@if($chart)
	<table>
		<tr>
			<th>Name : </th>
			<td>{{$chart->name}}</td>
		</tr>
		<tr>
			<th>Colors : </th>
			<td>
				<table>
				<tr>
					@foreach($chart->data['colors'] as $color)
						{{$color}}
					@endforeach
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<th>Datasets: </th>
			<td>
				<table>
				<tr>
					@foreach($chart->data['dataProvider'][0] as $h=>$v)
						<th>{{$h}}</th>
					@endforeach
				</tr>
					@foreach($chart->data['dataProvider'] as $provider)
						<tr>
							@foreach($provider as $h=>$v)
								<td>{{$v}}</td>
							@endforeach
						</tr>
					@endforeach
				</table>
			</td>
		</tr>
		<tr>
			<th>Created : </th>
			<td>{{$chart->created_at}}</td>
		</tr>
	</table>
@else
<h3>Not Found</h3>
@endif
<hr>
<br><br>
<a href="{{url('chart')}}">Home</a> | <a href="{{url('chart/all')}}">All charts</a>
</body>
</html>