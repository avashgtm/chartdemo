<!DOCTYPE html>
<html>
<head>
	<title>All chart</title>

</head>
<body>
@if($charts && $charts->count())
<table>
<tr>
	<th>Name</th>
	<th>Created</th>
</tr>
	@foreach($charts as $chart)
	<tr>
	<td><a href='{{url("chart/"."$chart->id")}}'>{{$chart->name}}</a></td>
	<td>{{$chart->created_at}}</td>
	</tr>
	@endforeach
</table>
@else
<h3>No any charts</h3>
@endif
<hr>
<br><br>
<a href="{{url('chart')}}">Home</a>
</body>
</html>