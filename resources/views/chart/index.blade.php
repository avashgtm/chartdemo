<!DOCTYPE html>
<html>
<head>
	<title>chart created with amCharts | amCharts</title>
	<meta name="description" content="chart created using amCharts live editor" />
	<meta name="csrf-token" content="{{csrf_token()}}">

	<!-- amCharts javascript sources -->
	<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
	<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>
	<script type="text/javascript" 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
	<style type="text/css">
		html,body{
			margin:0;
			padding:0;
			height: 100%;
		}
		.nav-holder{
			background: #ddd;
			position: absolute;
			width: 30px;
			height: 100%;

		}
		nav{
			padding:10px 5px;
		}
		nav img{
			width: 20px;
			height: auto;
			margin-bottom: 20px;
		}
		nav.options{
			top: 40%;
			border-top: 2px solid #ccc;
		}
		nav.options a{
			color: blue;
		}
		.main{
			width: 100%;
			height: 100%;

		}

		.main .droparea{
			margin-top: 10px;
			margin-left: 40px;
			width: 90%;
			height: 90%;
			display: inline-block;
			background: rgb(250,250,250);
			border: 1px solid #ccc;
		}

		.holder{
			width: 600px;
			height: 400px;
			padding: 10px;
			position: absolute;
			top:100px;
			left:300px;
			border:1px solid #888;

		}
		#chartDiv{
			width: 100%;
			height: 100%;
		}
	</style>

</head>
<body>
	<div class="nav-holder">
		<nav>
			<img src="{{url('chartassets/bar.png')}}" class="charts" title="column chart" data-type="col">
			<img src="{{url('chartassets/line.svg')}}" class="charts" title="line chart" data-type="line">
		</nav>
		<nav class="options">
			<a href="E" title="reset" id="reset"><img src="{{url('chartassets/reset.png')}}"></a>
			<a href="#" id="save" title="save"><img src="{{url('chartassets/save.svg')}}"></a>
		</nav>
	</div>
	<div class="main">
		<div class="droparea">
		</div>
		<br>
		<a href="{{url('chart/all')}}" style="margin-left: 40px" target="_blank">All charts</a>
	</div>
	<script>
		var url = "{{url('chart')}}";
	</script>
	<script type="text/javascript" src="{{url('chartassets/app.js')}}">
	</script>
</body>
</html>