<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('chart',function(){
	return view('chart.index');
});
Route::post('chart',function(Request $request){
	$chart = new App\Chart();
	$chart->name = $request->get('name');
	$chart->data = $request->get('data');
	$chart->save();
	return response()->json(['success']);
});
Route::get('chart/all',function(){
	$charts = App\Chart::orderBy('id','desc')->get();
	return view('chart.all')->with(['charts'=>$charts]);
});
Route::get('chart/{id}',function($id){
	$chart = App\Chart::find($id);
	return view('chart.single')->with(['chart'=>$chart]);
});
