		$(function(){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				contentType: "application/json; charset=utf-8"
			});
			var chart,saving=false;

			$("#reset").click(function(e){
				e.preventDefault();
				$('.droparea').html('');
				saving = false;
			});
			$("#save").click(function(e){
				e.preventDefault();
				var data,name;
				if(chart && saving==false ){
					name = prompt("Save as.......Enter name");
					if(name){
						data = {
							name:name,
							data:{
								colors:chart.colors,
								dataProvider:chart.dataProvider
							}
						};
						saving = true;
						$.post(url,JSON.stringify(data),function(response){
							alert('saved');
						});
					}

				}
			});
			var colOptions = {
				"type": "serial",
				"categoryField": "category",
				"startDuration": 1,
				"categoryAxis": {
					"gridPosition": "start"
				},
				"trendLines": [],
				"graphs": [
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 1,
					"id": "AmGraph-1",
					"title": "graph 1",
					"type": "column",
					"valueField": "column-1"
				},
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"fillAlphas": 1,
					"id": "AmGraph-2",
					"title": "graph 2",
					"type": "column",
					"valueField": "column-2"
				}
				],
				"guides": [],
				"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Axis title"
				}
				],
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true,
					"useGraphSettings": true
				},
				"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": "Chart Title"
				}
				],
				"dataProvider": [
				{
					"category": "category 1",
					"column-1": 8,
					"column-2": 5
				},
				{
					"category": "category 2",
					"column-1": 6,
					"column-2": 7
				},
				{
					"category": "category 3",
					"column-1": 2,
					"column-2": 3
				}
				]
			}
			var lineOptions = {
				"type": "serial",
				"categoryField": "category",
				"startDuration": 1,
				"categoryAxis": {
					"gridPosition": "start"
				},
				"trendLines": [],
				"graphs": [
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"bullet": "round",
					"id": "AmGraph-1",
					"title": "graph 1",
					"valueField": "column-1"
				},
				{
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"bullet": "square",
					"id": "AmGraph-2",
					"title": "graph 2",
					"valueField": "column-2"
				}
				],
				"guides": [],
				"valueAxes": [
				{
					"id": "ValueAxis-1",
					"title": "Axis title"
				}
				],
				"allLabels": [],
				"balloon": {},
				"legend": {
					"enabled": true,
					"useGraphSettings": true
				},
				"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": "Chart Title"
				}
				],
				"dataProvider": [
				{
					"category": "category 1",
					"column-1": 8,
					"column-2": 5
				},
				{
					"category": "category 2",
					"column-1": 6,
					"column-2": 7
				},
				{
					"category": "category 3",
					"column-1": 2,
					"column-2": 3
				},
				{
					"category": "category 4",
					"column-1": 1,
					"column-2": 3
				},
				{
					"category": "category 5",
					"column-1": 2,
					"column-2": 1
				},
				{
					"category": "category 6",
					"column-1": 3,
					"column-2": 2
				},
				{
					"category": "category 7",
					"column-1": 6,
					"column-2": 8
				}
				]
			};
			$('.charts').draggable({revert: "invalid",containment: "document",
				helper: "clone",
				cursor: "move"});
			$('.droparea')
			.droppable({
				accept: ".charts",
				classes: {
					"ui-droppable-active": "ui-state-highlight"
				},
				drop: function( event, ui ) {
					$(".droparea").html("<div class='holder'><div id='chartDiv'></div></div>");
					$('.holder')
					.draggable({ containment: "parent" })
					.resizable();
					var type = (ui.helper).attr('data-type');

					chartOptions = type=='col'?colOptions:lineOptions;
					chart = AmCharts.makeChart("chartDiv",chartOptions);
					saving = false;
					//console.log(a.colors);
				}
			})
		});